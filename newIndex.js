(function($, window) {

  window.StateManager.init([
    {
      state: 'xs',
      enter: 0,
      exit: 575
    },
    {
      state: 'sm',
      enter: 576,
      exit: 767
    },
    {
      state: 'md',
      enter: 768,
      exit: 991
    },
    {
      state: 'lg',
      enter: 992,
      exit: 1199
    },
    {
      state: 'xl',
      enter: 1200,
      exit: 1920
    },
    {
      state: 'xxl',
      enter: 1921,
      exit: 99999999
    }
  ]);
  window.StateManager.addPlugin('[data-googlemaps-address="true"]', 'wemGooglemapsAddress');

})(jQuery, window);
