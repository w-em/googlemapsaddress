Emiliano Florio

Aosta:
lat: 45.737615, lng: 7.324432

Turin: 
lat: 45.071368, lng: 7.589597

Cuneo: 
lat: 44.405976, lng: 7.530725

Verbano-Custio-Ossolo: 
lat: 46.100975, lng: 8.295858

Biella:
lat: 45.571335, lng: 8.062779

Vercelli:
lat: 45.323986, lng: 8.417888

Asti:
lat: 44.902463, lng: 8.210740

Alessandria:
lat: 44.919225, lng: 8.618028

Imperia:
lat: 43.889393, lng: 8.028129

Savona:
lat: 44.308229, lng: 8.469710

Genova:
lat: 44.409625, lng: 8.937171