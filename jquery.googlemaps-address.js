; (function ($, window) {
'use strict';
$.plugin('wemGooglemapsAddress', {
  defaults: {
    selector: '[data-controller="googlemaps-address"]'
  },
  init: function () {
    this.loadMaps();

    //// define all objects to handle in init ////
    this.nadellaGroupCountries = window.nadellaGroupCountries;
    this.distributionNetworks = window.distributionNetworks;
    this.durbalGroup = window.durbalGroup;
    this.metaData = window.metaData;

    this.officeMarkers = [];
    this.distNetworkMarkers = [];
    this.mainNav = document.querySelector("[data-controller='main-nav']");
    this.$officeList = document.querySelector("[data-controller='googlemaps-address/officeList']");
    this.$agentList = document.querySelector("[ data-controller='googlemaps-address/agentList']");
    this.$durbalAgentsList = document.querySelector("[ data-controller='googlemaps-address/durbalAgentsList']");
    this.$countryList = document.querySelector("[ data-controller='googlemaps-address/countrylist']");

    this.$nadellaGroupBtn = document.querySelector('#nadella-group.not-clicked');
    this.$distNetworkBtn = document.querySelector('#distribution-network.not-clicked');
    this.$durbalGroupBtn = document.querySelector('#durbal-group.not-clicked');
    this.$backToOffices = document.querySelector('#backToOffices');
    this.$backToCountries = document.querySelector('#backToCountries');
    this.$backToStart = document.querySelector('#backToStart');

    this.$dialogHeader = document.querySelector('.dialog-card-header');
    this.arrowBack = document.querySelector(".arrow-back");

    this.registerEvents();
  },
  loadMaps: function () {
    let me = this
    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyATt6ugQTIxeuqxMeIf1cPjJIFp9XT2k0g&libraries=&v=weekly';
    script.onload = function () {
      me.onMapsLoaded();
    };
    document.getElementsByTagName('head')[0].appendChild(script);
  },
  registerEvents: function () {
    if (this.mainNav) {
      //// EVENT DELEGATES ON #mainNav ELEMENT ////
      this.mainNav.addEventListener("click", $.proxy(this.onClickMainNavElements, this))
    }
    if (this.$dialogHeader) {
      //// EVENT DELEGATES ON BACK ARROW ELEMENT ////
      this.$dialogHeader.addEventListener("click", $.proxy(this.onClickBackButton, this))
    }
    if (this.$agentList) {
      this.$agentList.addEventListener("click", $.proxy(this.onClickAgent, this))
    }
    if (this.$officeList) {
      this.$officeList.addEventListener("click", $.proxy(this.onClickOffice, this))
    }
    if (this.$backToOffices) {
      this.$backToOffices.addEventListener("click", $.proxy(this.onClickBackToOffices, this))
    }
    if (this.$backToCountries) {
      this.$backToCountries.addEventListener("click", $.proxy(this.onClicKBackToCountries, this))
    }
    if (this.$backToStart) {
      this.$backToStart.addEventListener("click", $.proxy(this.onClickBackToStart, this))
    }
  },
  onClickMainNavElements: function (event) {
    /*--- CLICKED / NOT-CLICKED flags that are indicating if item should insert to the dom (only once) or should just trigger display properties to navigate
    through countries ---*/
    var map = this.map;
    if (!map) { return; }

    if (event.target && event.target.id === 'nadella-group' && event.target.classList.contains('not-clicked')) {
      this.onClickNadellaGroupButton(event);
    }
    if (event.target && event.target.id === 'nadella-group' && event.target.classList.contains('clicked')) {
      this.showNadellaCountries(event);
    }
    if (event.target && event.target.id === 'distribution-network' && event.target.classList.contains('not-clicked')) {
      this.onClickDistNetworkButton(event);
    }
    if (event.target && event.target.id === 'distribution-network' && event.target.classList.contains('clicked')) {
      this.showDistributionNetworkContinents(event);
    }
    if (event.target && event.target.id == 'durbal-group' && event.target.classList.contains('not-clicked')) {
      this.onClickDurbalGroupButton(event);
    }
    if (event.target && event.target.id == 'durbal-group' && event.target.classList.contains('clicked')) {
      this.showDurbalAgents(event);
    }
    if (event.target && event.target.className === 'nav-link not-clicked nadella-group') {
      this.insertOfficeItems(event);
    }
    if (event.target && event.target.className === 'nav-link not-clicked distribution-network-continent') {
      this.insertDistributionNetworkCountries(event);
    }
    if (event.target && event.target.className === 'nav-link distribution-network-continent clicked') {
      this.showDistributionNetworkCountries(event);
    }
    if (event.target && event.target.className === 'nav-link not-clicked distribution-network-country') {
      this.focusDistributionNetworkCountry(event);
      this.showCountryAgents(event);
    }
  },
  onClickAgent: function (event) {
    if (event.target && event.target.className.indexOf('agent-item') > -1) {
      this.removeUnselectedPolygons();
      var continentIndex = event.target.getAttribute('data-continent-index');
      var agentIndex = event.target.getAttribute('data-agent-index');
      var countryIndex = event.target.getAttribute('data-country-index');
      var selectedAgent = this.distributionNetworks[continentIndex].countries[countryIndex].agents[agentIndex];
      if (selectedAgent.polygon) {
        selectedAgent.polygon.setMap(this.map);
      }
    }
  },
  onClickOffice: function (event) {
    if (event.target && event.target.className.indexOf('office-item') > -1) {
      var countryIndex = event.target.getAttribute('data-country-index');
      var officeIndex = event.target.getAttribute('data-office-index');
      this.removeUnselectedOfficeLabels(countryIndex);
      var selectedOffice = this.nadellaGroupCountries[countryIndex].offices[officeIndex];
      selectedOffice.marker.setLabel({
        color: "#FEED02",
        fontWeight: "bold",
        text: selectedOffice.name
      })
    }
  },
  onClicKBackToCountries: function (event) {
    this.zoomOutMap();
    this.hideOfficeLabels();
    this.$officeList.classList.remove('visible');
    this.$countryList.classList.add('visible');
    this.$countryList.classList.remove('hidden');
    document.querySelector(".contact-dialog").classList.remove('contact-dialog-lg');
  },
  onClickNadellaGroupButton: function (event) {
    var map = this.map
    if (!map) { return; }
    this.showOfficeMarkers();
    this.arrowBack.classList.add('back-to-root')
    this.nadellaGroupCountries.forEach((country, i) => {
      this.mainNavUpdate(`${country.name}`, i, 'nadella-group');
    });
    this.clickFlagUpdate(event);
  },
  onClickDistNetworkButton: function (event) {
    var map = this.map;
    if (!map) { return; }
    this.showDistNetworkMarkers();
    this.arrowBack.classList.add('back-to-root')
    this.distributionNetworks.forEach((distNet, i) => {
      this.mainNavUpdate(`${distNet.continent}`, i, 'distribution-network-continent');
    })
    this.clickFlagUpdate(event);
  },
  onClickDurbalGroupButton: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault();
    map.fitBounds(this.durbalGroup.boundaries);
    this.durbalGroup.marker.setMap(this.map);
    this.arrowBack.classList.add('back-to-root');
    var agentItems = '';
    this.durbalGroup.agents_fields.forEach((field) => {
      var index = this.durbalGroup.agents_fields.indexOf(field);
      var item = '<div class="card">' +
        '<div class="card-header p-0" id="heading' + index + '">' +
        '<h2 class="mb-0">' +
        '<button class="btn btn-link btn-block text-left durbal-agent-item collapsed" data-controller="durbal-agent-btn" data-target="#collapse' + index + '" data-toggle="collapse" data-agent-index="' + index + '" aria-expanded="false" aria-controls="collapse' + index + '">' +
        field.expertise +
        '</button>' +
        '</h2>' +
        '</div>' +
        '<div id="collapse' + index + '" class="collapse" "data-controller="dialog/collapse" aria-labelledby="heading' + index + '" data-parent="#durbalAgents">' +
        '<div class="card-body">';
      field.agents.forEach((agent) => {
        item +=
        '<div style="padding: 10px;" >' +
          '<p>'+ '<b>' + metaData.agent_name + '</b>' + ':' + ' ' + agent.name + '</p>' +
          '<p>'+ '<b>' + metaData.phone + '</b>' + ':' + ' '+ agent.phone + '</p>' +
          '<p>'+ '<b>' + metaData.email + '</b>' + ':' + ' '+ agent.email + '</p>' +
        '</div>';
      })
      item += '</div>' +
        '</div>' +
        '</div>';
      agentItems += item;
    });
    document.querySelector('#durbalAgents').innerHTML = agentItems;
    this.$durbalAgentsList.classList.add('visible');
    this.clickFlagUpdate(event);
  },
  showDurbalAgents: function (event) {
    var map = this.map;
    this.$durbalAgentsList.classList.add('visible');
    map.fitBounds(this.durbalGroup.boundaries);
    this.durbalGroup.marker.setMap(this.map);
  },
  showNadellaCountries: function (event) {
    this.showOfficeMarkers();
    event.target.classList.add('hide-element');
    this.$distNetworkBtn.classList.add('hide-element');
    this.$durbalGroupBtn.classList.add('hide-element');
    this.arrowBack.classList.add('back-to-root');
    this.arrowBack.classList.remove('hide-element');
    this.showMultipleElements('nadella-group');
  },

  insertOfficeItems: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault();
    var countryIndex = event.target.getAttribute('data-index');
    var selectedCountry = this.nadellaGroupCountries[countryIndex];
    this.map.fitBounds(selectedCountry.boundaries);
    document.querySelector('.contact-dialog').classList.add('contact-dialog-lg');
    let officeItems = '';
    selectedCountry.offices.forEach((office) => {
      var index = selectedCountry.offices.indexOf(office);
      var item =
        '<div class="card">' +
        '<div class="card-header" id="heading' + index + '">' +
        '<h2 class="mb-0">' +
        '<button class="btn btn-link btn-block text-left office-item collapsed" type="button" data-toggle="collapse" data-office-index="' + index + '"data-country-index="' + countryIndex + '" data-target="#collapse' + index + '" aria-expanded="false" aria-controls="collapse' + index + '">' +
        office.name +
        '</button>' +
        '</h2>' +
        '</div>' +
        '<div id="collapse' + index + '" class="collapse" aria-labelledby="heading' + index + '" data-parent="#offices">' +
        '<div class="card-body">' +
        '<p>'+ '<b>' + metaData.address + '</b>' + ':' + ' ' + office.address + '</p>' +
        '<p>'+ '<b>' + metaData.phone + '</b>' + ':' + ' ' + office.phone + '</p>' +
        '<p>'+ '<b>' + metaData.fax + '</b>' + ':' + ' ' + (office.fax ? office.fax : ' / ') + '</p>' +
        '<p>'+ '<b>' + metaData.email + '</b>' + ':' + ' ' + office.email + '</p>' +
        '<p>'+ '<b>' + metaData.website + '</b>' + ':' + ' ' + office.website + '</p>' +
        '</div>' +
        '</div>' +
        '</div>';
      officeItems += item;
    })
    //// UPDATE UI ////
    document.querySelector('#offices').innerHTML = officeItems;
    this.$countryList.classList.add('hidden');
    this.$countryList.classList.remove('visible');
    this.$officeList.classList.add('visible');
  },
  insertDistributionNetworkCountries: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault();
    this.hideMultipleElements('distribution-network-continent');
    var continentIndex = event.target.getAttribute('data-index');
    var selectedContinent = this.distributionNetworks[continentIndex];
    selectedContinent.countries.forEach((country, i) => {
      this.mainNavUpdate(`${country.name}`, i, 'distribution-network-country', continentIndex);
    })
    this.clickFlagUpdate(event);
    this.arrowBack.classList.add("back-to-continents");
  },
  showDistributionNetworkContinents: function (event) {
    this.showDistNetworkMarkers();
    event.target.classList.add('hide-element');
    this.$nadellaGroupBtn.classList.add('hide-element');
    this.$durbalGroupBtn.classList.add('hide-element');
    this.arrowBack.classList.add('back-to-root');
    this.arrowBack.classList.remove('hide-element');
    this.showMultipleElements('distribution-network-continent');
  },
  showDistributionNetworkCountries: function (event) {
    this.hideMultipleElements('distribution-network-continent');
    var continentIndex = event.target.getAttribute('data-index');
    var selectedCountries = document.querySelectorAll(`[data-parent-index='${continentIndex}']`);
    this.arrowBack.classList.add('back-to-continents');
    for (var item of selectedCountries) {
      item.classList.remove('hide-element');
    }
  },
  focusDistributionNetworkCountry: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault();
    var countryContinentIndex = event.target.getAttribute('data-parent-index')
    var countryIndex = event.target.getAttribute('data-index');
    var selectedCountry = this.distributionNetworks[countryContinentIndex].countries[countryIndex];
    map.fitBounds(selectedCountry.boundaries);
  },
  showCountryAgents: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault();
    var countryIndex = event.target.getAttribute('data-index');
    var continentIndex = event.target.getAttribute('data-parent-index');
    var selectedCountry = this.distributionNetworks[continentIndex].countries[countryIndex];
    var agentItems = '';
    selectedCountry.agents.forEach((agent) => {
      var index = selectedCountry.agents.indexOf(agent);
      var item = '<div class="card">' +
        '<div class="card-header p-0" id="heading' + index + '">' +
        '<h2 class="mb-0">' +
        '<button class="btn btn-link btn-block text-left agent-item collapsed" data-controller="agent-btn" data-target="#collapse' + index + '" data-toggle="collapse" data-continent-index = "' + continentIndex + '" data-country-index="' + countryIndex + '" data-agent-index="' + index + '" aria-expanded="false" aria-controls="collapse' + index + '">' +
        (agent.sales_area_coordinates ? agent.sales_zone : selectedCountry.name) +
        '</button>' +
        '</h2>' +
        '</div>' +
        '<div id="collapse' + index + '" class="collapse ' + (agent.sales_area_coordinates ? '' : 'show') + '" "data-controller="dialog/collapse" aria-labelledby="heading' + index + '" data-parent="#agents">' +
        '<div class="card-body">' +
        '<p>'+ '<b>' + (agent.name ? metaData.agent_name + ': ' : '') + '</b>' + (agent.name ? agent.name : '') + '</p>' +
        '<p>'+ '<b>' + (agent.sales_zone ? metaData.sales_zone + ': ' : '') + '</b>' + (agent.sales_zone ? agent.sales_zone : ' /// ') + '</p>' +
        '<p>'+ '<b>' + (agent.phone ? metaData.phone + ': ' : '') + '</b>' + (agent.phone ? agent.phone : '') + '</p>' +
        '<p>'+ '<b>' + (agent.email ? metaData.email + ': ' : '') + '</b>' + (agent.email ? agent.email : '') + '</p>' +
        '<p>'+ '<b>' + (agent.website ? metaData.website + ': ' : '') + '</b>' + (agent.website ? agent.website : '') + '</p>' +
        '</div>' +
        '</div>' +
        '</div>';
      agentItems += item;
    })
    //// UPDATE UI ////
    document.querySelector('#agents').innerHTML = agentItems;
    this.$agentList.classList.add('visible');
  },
  backToContinents: function () {
    this.arrowBack.classList.remove("back-to-continents");
    this.hideMultipleElements('distribution-network-country');
    this.showMultipleElements('distribution-network-continent');
  },
  backToRoot: function () {
    var map = this.map;
    this.hideOfficeMarkers();
    this.hideDistNetworkMarkers();
    this.arrowBack.classList.remove("back-to-root");
    this.arrowBack.classList.add("hide-element");
    this.hideMultipleElements('nadella-group');
    this.hideMultipleElements('distribution-network-continent');
    this.hideMultipleElements()
    this.showMultipleElements('top-level');
  },
  onClickBackToOffices: function (event) {
    this.zoomOutMap();
    this.removeUnselectedPolygons();
    this.$agentList.classList.remove('visible');
    this.$officeList.classList.remove('hidden');
  },
  onClickBackToStart: function (event) {
    this.$durbalAgentsList.classList.remove('visible');
    this.durbalGroup.marker.setMap(null);
    this.zoomOutMap();
  },
  onClickBackButton: function (event) {
    var map = this.map;
    if (!map) { return; }
    event.preventDefault
    if (event.target && event.target.className.indexOf('back-to-root') > -1 && event.target.className.indexOf('back-to-continents') < 0) {
      this.backToRoot();
    }
    if (event.target && event.target.className.indexOf('back-to-continents') > -1) {
      this.backToContinents();
    }
  },
  zoomOutMap: function () {
    var map = this.map;
    map.panTo(new google.maps.LatLng(0, 0));
    map.setZoom(2);
  },
  clickFlagUpdate: function (event) {
    event.target.classList.remove("not-clicked");
    event.target.classList.add("clicked");
  },
  hideMultipleElements: function (className) {
    for (var item of document.querySelectorAll(`.${className}`)) {
      item.classList.add('hide-element')
    }
  },
  showMultipleElements: function (className) {
    for (var item of document.querySelectorAll(`.${className}`)) {
      item.classList.remove('hide-element')
    }
  },
  //// INSERT ELEMENTS IN #mainNav - DOM INSERTIONS ////
  mainNavUpdate: function (itemName, itemIndex, itemClass, parentIndex) {
    this.arrowBack.classList.remove('hide-element');
    this.hideMultipleElements('top-level');
    parentIndex = parentIndex || "no-parent";
    var li = document.createElement("li");
    li.textContent = itemName;
    li.setAttribute('data-index', itemIndex);
    li.classList.add('nav-link', 'not-clicked', itemClass);
    li.setAttribute('data-parent-index', parentIndex);
    this.mainNav.appendChild(li);
  },
  removeUnselectedPolygons: function () {
    var countriesWithAgents = this.distributionNetworks[0].countries.filter(country => country.agents !== undefined);
    countriesWithAgents.forEach(country => {
      country.agents.forEach((agent) => {
        if (agent.polygon) {
          agent.polygon.setMap(null)
        }
      });
    })
  },
  showOfficeMarkers: function () {
    this.nadellaGroupCountries.forEach(country => {
      country.offices.forEach(office => {
        office.marker.setMap(this.map);
      })
    })
  },
  hideOfficeMarkers: function () {
    this.nadellaGroupCountries.forEach(country => {
      country.offices.forEach(office => {
        office.marker.setMap(null);
      })
    })
  },
  showDistNetworkMarkers: function () {
    for (var marker of this.distNetworkMarkers) {
      marker.setMap(this.map);
    }
  },
  hideDistNetworkMarkers: function () {
    for (var marker of this.distNetworkMarkers) {
      marker.setMap(null);
    }
  },
  hideOfficeLabels: function () {
    this.nadellaGroupCountries.forEach(country => {
      country.offices.forEach(office => {
        office.marker.setLabel(null);
      })
    })
  },
  removeUnselectedOfficeLabels: function (countryIndex) {
    this.nadellaGroupCountries[countryIndex].offices.forEach(office => {
      office.marker.setLabel(null);
    })
  },
  /*--- FUNCTIONS BEING CALLED ON MAP INITIALIZATION ---*/
  setOfficeMarkers: function () {
    this.nadellaGroupCountries.forEach(country => {
      country.offices.forEach(office => {
        office.marker = new google.maps.Marker({
          position: office.position,
          title: office.name,
          icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
            labelOrigin: new google.maps.Point(15, -10)
          }
        })
      })
    })
  },
  setDistNetworkMarkers: function () {
    this.distributionNetworks.forEach(continent => {
      continent.countries.forEach(country => {
        this.distNetworkMarkers.push(new google.maps.Marker({
          position: country.marker_location,
          icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
            labelOrigin: new google.maps.Point(15, -10)
          }
        }))
      })
    })
  },
  setDurbalGroupMarker: function () {
    this.durbalGroup.marker = new google.maps.Marker({
      position: this.durbalGroup.position,
      icon: {
        url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
        labelOrigin: new google.maps.Point(15, -10)
      }
    })
  },
  setCountryBoundaries: function () {
    this.nadellaGroupCountries.forEach((country) => {
      country.boundaries = new google.maps.LatLngBounds(
        new google.maps.LatLng(country.southwest),
        new google.maps.LatLng(country.northeast)
      )
    })
    this.distributionNetworks.forEach((network) => {
      network.countries.forEach((country) => {
        country.boundaries = new google.maps.LatLngBounds(
          new google.maps.LatLng(country.southwest),
          new google.maps.LatLng(country.northeast)
        )
      })
    })
    this.durbalGroup.boundaries = new google.maps.LatLngBounds(
      new google.maps.LatLng(this.durbalGroup.southwest),
      new google.maps.LatLng(this.durbalGroup.northeast)
    )
  },
  setSalesAreas: function () {
    var countriesWithAgents = this.distributionNetworks[0].countries.filter(country => country.agents != undefined);
    countriesWithAgents.forEach(country => {
      country.agents.forEach(agent => {
        if (agent.sales_area_coordinates) {
          agent.polygon = new google.maps.Polygon({
            paths: agent.sales_area_coordinates,
            strokeColor: "#fffb00",
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: "#fffb00",
            fillOpacity: 0.2,
          })
        }
      })
    })
  },
  onMapsLoaded: function () {
    var me = this
    this.map = new google.maps.Map(document.querySelector("*[data-controller=\"googlemaps-address/map\"]"), {
      zoom: 2,
      center: new google.maps.LatLng(0, 0),
      styles: window.mapStyle
    });
    this.setCountryBoundaries();
    this.setSalesAreas();
    this.setOfficeMarkers();
    this.setDistNetworkMarkers();
    this.setDurbalGroupMarker();
  }
});
})(jQuery, window);
