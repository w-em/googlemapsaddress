var durbalGroup = {
    position: {
      lat: 49.196239488637936,
      lng: 9.493850113915144
    },
    southwest: {
      lat: 47.271772,
      lng: 5.950244
    },

    northeast: {
      lat: 54.907933,
      lng: 15.032822
    },
    agents_fields: [
      {
        expertise: "Customer Service Manager",
        agents: [{
          name: "Pieter Höning",
          phone: "+49 (0) 7941 9460-31",
          email: "Pieter.hoening@durbal.de"
        }]
      },
      {
        expertise: "Sales",
        agents: [
          {
            name: "Christina El Kourouchi",
            phone: "+49 (0) 7941 9460-32",
            email: "Christina.ElKourouchi@durbal.de"
          },
          {
            name: "Simone Dörre",
            phone: "+49 (0) 7941 9460-29",
            email: "Simone.doerre@durbal.de"
          }
        ]
      },
      {
        expertise: "Export",
        agents: [
          {
            name: "Caroline Vermeulen",
            phone: "+49 (0) 7941 9460-33",
            email: "Caroline.vermeulen@durbal.de"
          },
          {
            name: "Sabine Ratz",
            phone: "+49 (0) 7941 9460-48",
            email: "Sabine.ratz@durbal.de"
          }
        ]
      },
      {
        expertise: "Brand Sales Manager",
        agents: [
          {
            name: "Carlo Rumore",
            phone: "+49 (0) 7941 9460-30",
            email: "Carlo.rumore@durbal.de"
          }
        ]
      }
    ]
  }