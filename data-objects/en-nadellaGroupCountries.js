var nadellaGroupCountries = [
    {
      name: "Italy",
      southwest: {
        lat: 36.653579,
        lng: 6.643533
      },
      northeast: {
        lat: 47.085868,
        lng: 13.894082
      },
      offices: [
        {
          name: "Nadella S.p.A. Italy",
          address: "Via Melette 16, 20128 Milano",
          phone: "+39 02 27 093",
          fax: "+39 02 257 64 79",
          email: "customer.service@nadella.it",
          website: "www.nadella.it",
          position: {
            lat: 45.507272,
            lng: 9.232460
          },
        },
        {
          name: "Chiavette Unificate S.p.A. Italy",
          address: "Via G.Brodolini 6-8-10, 40069 Zola Predosa, Bologna",
          phone: "+39 051 75 87 67",
          fax: "+39 051 75 47 80",
          email: "tescubal@chiavette.it",
          website: "www.chiavette.com",
          position: {
            lat: 44.494927,
            lng: 11.250934
          },
        }
      ]

    },
    {
      name: "France",
      southwest: {
        lat: 42.344824,
        lng: -4.757775
      },
      northeast: {
        lat: 51.087416,
        lng: 8.189850
      },
      offices: [
        {
          name: "NADELLA Sarl France",
          address: "12 Parvis Colonel Arnaud Beltrame, Hall A 4ème étage, 78000 Versailles",
          phone: "+33 (0)1 7319 4048",
          fax: null,
          email: "service.client@nadella.fr",
          website: "www.nadella.fr",
          position: {
            lat: 48.795942,
            lng: 2.135569

          }
        }
      ]
    },
    {
      name: "Spain",
      southwest: {
        lat: 36.032485,
        lng: -7.391708
      },
      northeast: {
        lat: 43.445952,
        lng: 3.138594
      },
      offices: [
        {
          name: "SHUTON S.A. Spain",
          address: "Polígono Industrial Goian, C/Subinoa, 5 - 01170 LEGUTIANO",
          phone: "+34 945 465 629",
          fax: "+34 945 465 610",
          email: "shuton@shuton.com",
          website: "www.shuton.com",
          position: {
            lat: 42.944082,
            lng: -2.647418
          },
        },
        {
          name: "HUSILLOS IPIRANGA Spain",
          address: "Polígono Industrial Erratzu, Parcela G3. Pabellón 221 - Apdo.65, E-20130 Urnieta - Gipuzkoa",
          phone: "+34 943 336 370",
          fax: null,
          email: "info@iazgroup.net",
          website: "www.ipirangahusillos.com",
          position: {
            lat: 43.2388414,
            lng: -1.9947043
          }
        }
      ]
    },
    {
      name: "Germany",
      southwest: {
        lat: 47.271772,
        lng: 5.950244
      },
      northeast: {
        lat: 54.907933,
        lng: 15.032822
      },
      offices: [
        {
          name: "NADELLA GmbH Germany",
          address: "Rudolf-Diesel-Str. 28, 71154 Nufringen",
          phone: "+49 7032 9540-0",
          fax: "+49 7032 9540-25",
          email: "info@nadella.de",
          website: "www.nadella.de",
          position: {
            lat: 48.631283,
            lng: 8.895013
          },
        },
        {
          name: "DURBAL Metallwarenfabrik GmbH Germany",
          address: "Verrenberger Weg 2, 74613 Öhringen",
          phone: "+49 7941 9460-0",
          fax: "+49 7941 9460-90",
          email: "info@durbal.de",
          website: "www.durbal.de",
          position: {
            lat: 49.196230,
            lng: 9.493846
          }
        }
      ]
    },
    {
      name: "China",
      southwest: {
        lat: 21.634673,
        lng: 73.749601
      },
      northeast: {
        lat: 42.483879,
        lng: 122.265221
      },
      offices: [
        {
          name: "NADELLA Linear Shanghai Co. Ltd. China",
          address: "No. 245 Xinjuanhuan Road, Shanghai 201114",
          phone: "+86 21 5068 3835",
          fax: "+86 21 5038 7725",
          email: "info@nadellalinear.com",
          website: "www.nadella.cn.com",
          position: {
            lat: 31.2243085,
            lng: 120.9162948

          }
        }
      ],
    },
    {
      name: "United States",
      southwest: {
        lat: 26.375012,
        lng: -171.673077
      },
      northeast: {
        lat: 71.123237,
        lng: -67.744991
      },
      offices: [
        {
          name: "Nadella Inc.United States",
          address: "14115 – 63 Way North, Clearwater – Florida 33760-3621",
          phone: "+1 844-537-0330 (toll-free)",
          fax: "+1 844-537-0331",
          email: "info@nadella.com",
          website: "www.nadella.com",
          position: {
            lat: 27.8999799,
            lng: -82.7252992,
          }
        }
      ]
    }
  ];