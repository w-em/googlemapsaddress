
function initMap() {
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 2,
        center: new google.maps.LatLng(0, 0)
    });

    const nadellaGroupCountries = [
        {
            name: "Italy",
            southwest: {
                lat: 36.653579,
                lng: 6.643533
            },
            northeast: {
                lat: 47.085868,
                lng: 13.894082
            },
            offices: [
                {
                    name: "Nadella S.p.A. Italy",
                    address: "Via Melette 16, 20128 Milano",
                    phone: "+39 02 27 093",
                    fax: "+39 02 257 64 79",
                    email: "customer.service@nadella.it",
                    website: "www.nadella.it",
                    position: {
                        lat: 45.507272,
                        lng: 9.2302726
                    },
                },
                {
                    name: "Chiavette Unificate S.p.A. Italy",
                    address: "Via G.Brodolini 6-8-10, 40069 Zola Predosa, Bologna",
                    phone: "+39 051 75 87 67",
                    fax: "+39 051 75 47 80",
                    email: "tescubal@chiavette.it",
                    website: "www.chiavette.com",
                    position: {
                        lat: 44.4945745,
                        lng: 11.2488201
                    },
                }
            ]

        },
        {
            name: "France",
            southwest: {
                lat: 42.344824,
                lng: -4.757775
            },
            northeast: {
                lat: 51.087416,
                lng: 8.189850
            },
            offices: [
                {
                    name: "NADELLA Sarl France",
                    address: "12 Parvis Colonel Arnaud Beltrame, Hall A 4ème étage, 78000 Versailles",
                    phone: "+33 (0)1 7319 4048",
                    fax: null,
                    email: "service.client@nadella.fr",
                    website: "www.nadella.fr",
                    position: {
                        lat: 48.7959191,
                        lng: 2.1333721

                    }
                }
            ]
        },
        {
            name: "Spain",
            southwest: {
                lat: 36.032485,
                lng: -7.391708
            },
            northeast: {
                lat: 43.445952,
                lng: 3.138594
            },
            offices: [
                {
                    name: "SHUTON S.A. Spain",
                    address: "Polígono Industrial Goian, C/Subinoa, 5 - 01170 LEGUTIANO",
                    phone: "+34 945 465 629",
                    fax: "+34 945 465 610",
                    email: "shuton@shuton.com",
                    website: "www.shuton.com",
                    position: {
                        lat: 42.9483383,
                        lng: -2.6413685
                    },
                },
                {
                    name: "HUSILLOS IPIRANGA Spain",
                    address: "Polígono Industrial Erratzu, Parcela G3. Pabellón 221 - Apdo.65, E-20130 Urnieta - Gipuzkoa",
                    phone: "+34 943 336 370",
                    fax: null,
                    email: "info@iazgroup.net",
                    website: "www.ipirangahusillos.com",
                    position: {
                        lat: 43.2388414,
                        lng: -1.9947043
                    }
                }
            ]
        },
        {
            name: "Germany",
            southwest: {
                lat: 47.271772,
                lng: 5.950244
            },
            northeast: {
                lat: 54.907933,
                lng: 15.032822
            },
            offices: [
                {
                    name: "NADELLA GmbH Germany",
                    address: "Rudolf-Diesel-Str. 28, 71154 Nufringen",
                    phone: "+49 7032 9540-0",
                    fax: "+49 7032 9540-25",
                    email: "info@nadella.de",
                    website: "www.nadella.de",
                    position: {
                        lat: 48.631223,
                        lng: 8.8928462
                    },
                },
                {
                    name: "DURBAL Metallwarenfabrik GmbH Germany",
                    address: "Verrenberger Weg 2, 74613 Öhringen",
                    phone: "+49 7941 9460-0",
                    fax: "+49 7941 9460-90",
                    email: "info@durbal.de",
                    website: "www.durbal.de",
                    position: {
                        lat: 49.196227,
                        lng: 9.4916576
                    }
                }
            ]
        },
        {
            name: "China",
            southwest: {
                lat: 21.634673,
                lng: 73.749601
            },
            northeast: {
                lat: 42.483879,
                lng: 122.265221
            },
            offices: [
                {
                    name: "NADELLA Linear Shanghai Co. Ltd. China",
                    address: "No. 245 Xinjuanhuan Road, Shanghai 201114",
                    phone: "+86 21 5068 3835",
                    fax: "+86 21 5038 7725",
                    email: "info@nadellalinear.com",
                    website: "www.nadella.cn.com",
                    position: {
                        lat: 31.2243085,
                        lng: 120.9162948

                    }
                }
            ],
        },
        {
            name: "United States",
            southwest: {
                lat: 26.375012,
                lng: -171.673077
            },
            northeast: {
                lat: 71.123237,
                lng: -67.744991
            },
            offices: [
                {
                    name: "Nadella Inc.United States",
                    address: "14115 – 63 Way North, Clearwater – Florida 33760-3621",
                    phone: "+1 844-537-0330 (toll-free)",
                    fax: "+1 844-537-0331",
                    email: "info@nadella.com",
                    website: "www.nadella.com",
                    position: {
                        lat: 27.8999799,
                        lng: -82.7252992,
                    }
                }
            ]
        }
    ]
    const distributionNetworks = [
        {
            continent: "Europe",
            countries: [
                {
                    name: "Austria",
                    marker_location: {
                        lat: 47.541286,
                        lng: 14.332802
                    }
                },
                {
                    name: "Belgium",
                    marker_location: {
                        lat: 50.82963,
                        lng: 4.422348
                    }
                },
                {
                    name: "Czech Republic",
                    marker_location: {
                        lat: 49.965183,
                        lng: 14.544540
                    }
                },
                {
                    name: "Denmark",
                    marker_location: {
                        lat: 55.375197,
                        lng: 10.243975
                    }
                },
                {
                    name: "Finland",
                    marker_location: {
                        lat: 65.426445,
                        lng: 27.138319
                    }
                },
                {
                    name: "France",
                    southwest: {
                        lat: 42.344824,
                        lng: -4.757775
                    },
                    northeast: {
                        lat: 51.087416,
                        lng: 8.189850
                    },
                    marker_location: {
                        lat: 47.306620,
                        lng: 2.860609
                    }
                },
                {
                    name: "Germany",
                    southwest: {
                        lat: 47.271772,
                        lng: 5.950244
                    },
                    northeast: {
                        lat: 54.907933,
                        lng: 15.032822
                    },
                    marker_location: {
                        lat: 50.854683,
                        lng: 10.472044
                    }
                },
                {
                    name: "Great Britain"
                },
                {
                    name: "Hungary"
                },
                {
                    name: "Italy",
                    southwest: {
                        lat: 36.653579,
                        lng: 6.643533
                    },
                    northeast: {
                        lat: 47.085868,
                        lng: 13.894082
                    },
                    marker_location: {
                        lat: 43.110107,
                        lng: 12.380734
                    }
                },
                {
                    name: "Netherlands"
                },
                {
                    name: "Norway"
                },
                {
                    name: "Poland"
                },
                {
                    name: "Portugal"
                },
                {
                    name: "Romania"
                },
                {
                    name: "Slovakia"
                },
                {
                    name: "Spain",
                    southwest: {
                        lat: 36.032485,
                        lng: -7.391708
                    },
                    northeast: {
                        lat: 43.445952,
                        lng: 3.138594
                    },
                    marker_location: {
                        lat: 40.317811,
                        lng: -3.973614
                    }
                },
                {
                    name: "Sweden"
                },
                {
                    name: "Switzerland"
                },
                {
                    name: "Turkey"
                },
                {
                    name: "Ukraine"
                }
            ]
        },
        {
            continent: "Asia",
            countries: [
                {
                    name: "China"
                },
                {
                    name: "Hong Kong"
                },
                {
                    name: "India"
                },
                {
                    name: "Korea"
                },
                {
                    name: "Singapore"
                },
                {
                    name: "Taiwan"
                }
            ]
        },
        {
            continent: "South America"
        },
        {
            continent: "USA"
        }
    ]

    //// ***---SHOULD BE SUITED FOR GENERAL USE---*** ////
    const europeanDist = distributionNetworks.filter((network) => network.continent === "Europe")[0];
    for (const country of europeanDist.countries) {
        country.boundaries = new google.maps.LatLngBounds(
            new google.maps.LatLng(country.southwest),
            new google.maps.LatLng(country.northeast)
        )
    }

    const mainNav = document.querySelector("#mainNav");
    const nadellaGroupBtn = document.querySelector('#nadella-group.not-clicked');
    const distNetworkBtn = document.querySelector('#distribution-network.not-clicked');
    const dialogHeader = document.querySelector('.dialog-card-header');

    //// SETTING COUNTRY BOUNDARIES BASED ON southwest & northeast POSITIONAL VALUES ////
    nadellaGroupCountries.forEach((country) => {
        country.boundaries = new google.maps.LatLngBounds(
            new google.maps.LatLng(country.southwest),
            new google.maps.LatLng(country.northeast)
        )
    })

    //// INITIAL NADELLA GROUP BUTTON - LIST COUNTRIES AND ADD MARKERS ////
    nadellaGroupBtn.addEventListener("click", (event) => {
        nadellaGroupCountries.forEach((country, i) => {
            mainNavUpdate(`${country.name}`, i, 'nadella-group');
            country.offices.forEach((office) => {
                office.location = new google.maps.Marker({
                    position: office.position,
                    title: office.name,
                    map,
                    icon: {
                        url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
                        labelOrigin: new google.maps.Point(15, -10)
                    }
                });
            });
        });
        clickFlagUpdate(event);
    })

    //// INITIAL DISTRIBUTION NETWORK BUTTON - LIST CONTINENTS ////
    distNetworkBtn.addEventListener("click", (event) => {
        console.log("Hello");
        distributionNetworks.forEach((distNet, i) => {
            mainNavUpdate(`${distNet.continent}`, i, 'distribution-network-continent');
        })
    })

    //// NAVIGATION UPDATES - EVENTS DELEGATION ////
    mainNav.addEventListener('click', (event) => {

        //// NADELLA GROUP COUNTRIES ////
        if (event.target && event.target.className === 'nav-link not-clicked nadella-group') {
            event.preventDefault();
            const countryIndex = event.target.getAttribute('data-index');
            const selectedCountry = nadellaGroupCountries[countryIndex];
            map.fitBounds(selectedCountry.boundaries);
            document.querySelector('.contact-dialog').classList.add('contact-dialog-lg');
            let officeItems = '';
            selectedCountry.offices.forEach((office) => {
                office.location.setLabel({
                    color: "#FEED02",
                    fontWeight: "bold",
                    text: office.location.title
                });
                const index = selectedCountry.offices.indexOf(office);
                const item =
                    '<div class="card">' +
                    '<div class="card-header" id="heading' + index + '">' +
                    '<h2 class="mb-0">' +
                    '<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse' + index + '" aria-expanded="false" aria-controls="collapse' + index + '">' +
                    office.name +
                    '</button>' +
                    '</h2>' +
                    '</div>' +
                    '<div id="collapse' + index + '" class="collapse" aria-labelledby="heading' + index + '" data-parent="#offices">' +
                    '<div class="card-body">' +
                    '<p><b>Address: </b>' + office.address + '</p>' +
                    '<p><b>Phone: </b>' + office.phone + '</p>' +
                    '<p><b>Fax: </b>' + (office.fax ? office.fax : ' / ') + '</p>' +
                    '<p><b>Email: </b>' + office.email + '</p>' +
                    '<p><b>Website: </b>' + office.website + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                officeItems += item;
            })
            document.querySelector('#offices').innerHTML = officeItems;
            document.querySelector('#countryList').classList.add('hidden');
            document.querySelector('#countryList.visible').classList.remove('visible');
            document.querySelector('#officeList').classList.add('visible');
        }

        //// DISTRIBUTION NETWORK CONTINENTS ////
        else if (event.target && event.target.className === 'nav-link not-clicked distribution-network-continent') {
            event.preventDefault();
            const continents = document.querySelectorAll('.distribution-network-continent');
            for (const continent of continents) {
                continent.classList.add("hide-element");
            }
            const continentIndex = event.target.getAttribute('data-index');
            const selectedContinent = distributionNetworks[continentIndex];
            selectedContinent.countries.forEach((country, i) => {
                mainNavUpdate(`${country.name}`, i, 'distribution-network-country', continentIndex);
            })
            clickFlagUpdate(event);
            document.querySelector('.arrow-back').classList.add("back-to-continents");
        }

        //// DISTRIBUTION NETWORK COUNTRIES ////
        else if (event.target && event.target.className === 'nav-link not-clicked distribution-network-country') {
            event.preventDefault();
            const countryContinentIndex = event.target.getAttribute('data-parent-index')
            const countryIndex = event.target.getAttribute('data-index');
            const selectedCountry = distributionNetworks[countryContinentIndex].countries[countryIndex];
            map.fitBounds(selectedCountry.boundaries);
            selectedCountry.location = new google.maps.Marker({
                position: selectedCountry.marker_location,
                map,
                icon: {
                    url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
                    labelOrigin: new google.maps.Point(15, -10)
                }
            })
        }
    });
    dialogHeader.addEventListener("click", (event) => {
        if (event.target && event.target.className === 'material-icons arrow-back back-to-continents') {
            const distNetworkCountries = document.querySelectorAll('.distribution-network-country');
            for (const country of distNetworkCountries) {
                country.classList.add('hide-element');
            }
            const distNetworkContinents = document.querySelectorAll('.distribution-network-continent');
            for (const continent of distNetworkContinents) {
                continent.classList.remove('hide-element');
            }
        }
    })
}

//// DOM INSERTION IN #mainNav EVENT LISTENER DELEGATIONS ////
const mainNavUpdate = (itemName, itemIndex, itemClass, additionalIndex) => {
    document.querySelector('.arrow-back').classList.remove('hide-element');
    const topLevelitems = document.querySelectorAll('.top-level');
    for (const item of topLevelitems) {
        item.classList.add("hide-element");
    }
    additionalIndex = additionalIndex || "no-parent";
    const li = document.createElement("li");
    li.textContent = itemName;
    li.setAttribute('data-index', itemIndex);
    li.classList.add('nav-link', 'not-clicked', itemClass);
    li.setAttribute('data-parent-index', additionalIndex);
    mainNav.appendChild(li);
}

//// SWITCHES THE FLAG ON ELEMENTS AFTER INSERTING TO THE DOM - PREVENTS MULTIPLE DOM INSERTIONS ////
const clickFlagUpdate = (event) => {
    event.target.classList.remove("not-clicked");
    event.target.classList.add("clicked");
}